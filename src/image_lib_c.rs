use std::ffi::CString;
use std::path::PathBuf;

#[link(name = "my_ppma_io")]
extern "C" {
    fn ppma_read(
        input_name: *const i8,
        xsize: &mut i32,
        ysize: &mut i32,
        rgb_max: &mut i32,
        r: *mut *mut i32,
        g: *mut *mut i32,
        b: *mut *mut i32,
    );

    fn ppma_write(
        file_out_name: *const i8,
        xsize: i32,
        ysize: i32,
        r: *mut i32,
        g: *mut i32,
        b: *mut i32,
    );
}

pub struct ImageLibC {
    xsize: i32,
    ysize: i32,
    r: *mut i32,
    g: *mut i32,
    b: *mut i32,
}

impl ImageLibC {
    pub fn new_with_file(filename: PathBuf) -> ImageLibC {
        let input_name = CString::new(filename.to_str().unwrap()).expect("CString::new failed");
        let mut xsize: i32 = 0;
        let mut ysize: i32 = 0;
        let mut rgb_max: i32 = 0;
        let mut r: *mut i32 = std::ptr::null_mut();
        let mut g: *mut i32 = std::ptr::null_mut();
        let mut b: *mut i32 = std::ptr::null_mut();
        unsafe {
            ppma_read(
                input_name.as_ptr(),
                &mut xsize,
                &mut ysize,
                &mut rgb_max,
                &mut r,
                &mut g,
                &mut b,
            );
        }
        ImageLibC {
            xsize,
            ysize,
            r,
            g,
            b,
        }
    }

    pub fn save(&self, filename: PathBuf) {
        let output_name = CString::new(filename.to_str().unwrap()).expect("CString::new failed");
        unsafe {
            ppma_write(
                output_name.as_ptr(),
                self.xsize,
                self.ysize,
                self.r,
                self.g,
                self.b,
            )
        }
    }
}
