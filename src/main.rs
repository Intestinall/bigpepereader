#![feature(test)]
extern crate clap;
extern crate libc;
extern crate pyo3;
extern crate test;

mod command_line;
pub mod image;
mod image_lib_c;
mod line_of_pixel;
mod pixel;
mod utils;

use crate::command_line::Opts;
use image::Image;
use image_lib_c::ImageLibC;

fn main() {
    let opts: Opts = Opts::parse();

    match opts.function {
        choice @ 1..=3 => {
            let mut image = match choice {
                1 => Image::new_with_file,
                2 => Image::par_new_with_file,
                3 => Image::par_new_with_file2,
                _ => unreachable!("Already bounded by choice pattern range"),
            }(opts.image_path);

            if opts.grayscale {
                image.grayscale()
            }
            if opts.invert {
                image.invert()
            }
            image.save(opts.output_path);
        }
        4 => {
            let image = ImageLibC::new_with_file(opts.image_path);
            image.save(opts.output_path);
        }
        _ => unreachable!("Already bounded by function input check"),
    }
}
