use clap::Clap;
use std::path::PathBuf;

#[derive(Clap)]
#[clap(version = "1.0", author = "Uganda Knuckle")]
pub struct Opts {
    /// Path to the ppm file to edit
    #[clap(parse(from_os_str))]
    pub image_path: PathBuf,
    /// Path to output file
    #[clap(parse(from_os_str))]
    pub output_path: PathBuf,
    /// Function used to process the image
    /// It can be :
    /// 1 => new_with_file,
    /// 2 => par_new_with_file,
    /// 3 => par_new_with_file2,
    /// 4 => new_with_file (ppma_io Clib)
    #[clap(short = "f", long = "function", default_value = "1", parse(try_from_str = function_value_check))]
    pub function: u8,
    /// Apply grayscale to the colors
    #[clap(short = "g", long = "grayscale", parse(from_flag))]
    pub grayscale: bool,
    /// Invert the colors
    #[clap(short = "i", long = "invert", parse(from_flag))]
    pub invert: bool,
}

fn function_value_check(input: &str) -> Result<u8, String> {
    match input.parse() {
        Ok(v) => match v {
            v @ 1..=4 => Ok(v),
            _ => Err("function value must be : 1, 2, 3 or 4".to_string()),
        },
        Err(e) => Err(e.to_string()),
    }
}
