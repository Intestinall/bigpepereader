use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
mod image;
mod line_of_pixel;
mod pixel;
mod utils;
use image::Image;
use std::path::PathBuf;

#[pyfunction]
//#[args(grayscale = false, invert = false)]
fn convert_image(input_path: String, output_path: String, grayscale: bool, invert: bool) {
    let mut in_ = PathBuf::new();
    in_.push(input_path);

    let mut image = Image::new_with_file(in_);

    if grayscale {
        image.grayscale()
    }
    if invert {
        image.invert()
    }

    let mut out = PathBuf::new();
    out.push(output_path);

    image.save(out);
}

#[pymodule]
fn bigpepereader(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(convert_image))?;
    Ok(())
}
