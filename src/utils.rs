use std::fmt::Display;
use std::process::exit;

pub fn unwrap_or_exit<T, E: Display>(r: Result<T, E>) -> T {
    match r {
        Ok(v) => v,
        Err(e) => {
            println!("{}", e);
            exit(1)
        }
    }
}
