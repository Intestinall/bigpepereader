from bigpepereader import convert_image
from pathlib import Path

convert_image(
    str(Path("images", "big_pepe_for_the_queen_P3.ppm")),
    str(Path("images", "big_pepe_for_the_queen_P3_NEW.ppm")),
    invert=True,
    grayscale=False
)